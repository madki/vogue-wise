echo "emptying dist"
for l in $(ls dist/);
do
    rm -r dist/$l;
done

echo "copying to dist"
cp index.html dist/
cp -r images dist/
cp -r fonts dist/
cp -r js dist/
cp -r stylesheets dist/
echo "done copying"

echo "commiting changes in dist"
cd dist
git checkout gh-pages
git add . --all
git commit -m "regenerating dist"
git push origin gh-pages
echo "done pushing dist to gh-pages"
